(function($) {

    $(document).ready(function() {

    	// your code here
    	$.noConflict();

        // homepage content slider 
        if($().flexslider){
            $('.flexslider').flexslider({
                    animation: "slide",
                    controlNav: false,
                    slideshow: false,
            });
        }

        causeRepaintsOn = $("h1, h2, h3, p");

        $(window).resize(function() {
          causeRepaintsOn.css("z-index", 1);
        });

        // select2
        if ($().select2) {
            $('select.selectcustom').each(function() {
                $(this).select2({
                    minimumResultsForSearch: -1,
                });
            })
        }

        // hoverizr
        if($().hoverizr){

            $('.blur-fx').hoverizr({
                effect  : "blur",
                width   : "100%",
            });
        }

        // MatchHeight
        if($().matchHeight){

            $('.col-tiles').each(function() {
                $(this).children('.tiles-detail').matchHeight(true);
                console.log($(this).children('.boxed'));
            });

            $('.list-contact').each(function(){
                $(this).children('.list-wrapper').matchHeight(true);
            });

            if($('.list-wrapper').length){
                $('.list-wrapper').each(function(){
                    $(this).children('.list').matchHeight(true);
                });
            }
        }


    });

})(jQuery);